import 'dart:async';
import 'package:google_map_polyutil/google_map_polyutil.dart';
import 'package:location/location.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:toast/toast.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Google Maps Demo',
      home: MapSample(),
    );
  }
}

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  final Set<Polygon> _polygons = {};
  final Set<Marker> _markers = {};
  final List<PolygonId> polygonId = null;
  LocationData locationData;
  Completer<GoogleMapController> _controller = Completer();

  @override
  Widget build(BuildContext context) {
    CameraPosition _kGooglePlexInicaial = CameraPosition(
      target: LatLng(3.427363561012489, -76.52329596989264),
      zoom: 14,
    );

    final PolygonId polygonId1 = PolygonId('zona1 Cali');
    List<LatLng> listPointZona1 = [];
    listPointZona1.add(LatLng(3.450690, -76.537593));
    listPointZona1.add(LatLng(3.446318, -76.536368));
    listPointZona1.add(LatLng(3.452509, -76.505265));
    listPointZona1.add(LatLng(3.478260, -76.517082));

    final PolygonId polygonId2 = PolygonId('zona2 Cali');
    List<LatLng> listPointZona2 = [];
    listPointZona2.add(LatLng(3.4169477123360643, -76.52100719511509));
    listPointZona2.add(LatLng(3.4114465599864814, -76.5167012438178));
    listPointZona2.add(LatLng(3.4285918311950705, -76.49941440671682));
    listPointZona2.add(LatLng(3.4345647754421273, -76.52030244469643));

    final PolygonId polygonId3 = PolygonId('zona3 Palmira');
    List<LatLng> listPointZona3 = [];
    listPointZona3.add(LatLng(3.5475425956770503,-76.31482880562544));
    listPointZona3.add(LatLng(3.538987646336074,-76.31334319710732));
    listPointZona3.add(LatLng(3.538759424034896,-76.29791479557753));
    listPointZona3.add(LatLng(3.5540063570325002,-76.2946005910635));

    final PolygonId polygonId4 = PolygonId('zona4 Palmira');
    List<LatLng> listPointZona4 = [];
    listPointZona4.add(LatLng(3.524313708183174,-76.30545984953642));
    listPointZona4.add(LatLng(3.512372264594454,-76.30134500563146));
    listPointZona4.add(LatLng(3.5224895725618044,-76.28503818064928));
    listPointZona4.add(LatLng(3.5292981882095087,-76.29277400672436));

    return new Scaffold(
      body: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: _kGooglePlexInicaial,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
          _onAddPolygon(listPointZona1, polygonId1, 47, 68, 238);
          _onAddPolygon(listPointZona2, polygonId2, 96, 213, 49);
          _onAddPolygon(listPointZona3, polygonId3, 47, 68, 238);
          _onAddPolygon(listPointZona4, polygonId4, 96, 213, 49);
        },
        onTap: (value) {
          _onAddMarker(LatLng(value.latitude, value.longitude), "cliente");
          validateCustomerZone(context, value);
        },
        markers: _markers,
        polygons: _polygons,
        myLocationEnabled: false,
        indoorViewEnabled: false,
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => localizarme(),
        label: Text('Mí Ubicación!'),
        icon: Icon(Icons.gps_fixed),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  void _onAddMarker(LatLng latlang, String id) {
    setState(() {
      _markers.add(Marker(
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId(id),
        position: latlang,
        draggable: true,
        onDragEnd: (value) {
          print("--------> ${value.latitude},${value.longitude}");
          validateCustomerZone(context, value);
        },
        icon: BitmapDescriptor.defaultMarker,
      ));
    });
  }

  void _onAddPolygon(List<LatLng> latlang, PolygonId id, int r, int g, int b) {
    setState(() {
      _polygons.add(Polygon(
        // This marker id can be anything that uniquely identifies each marker.
        polygonId: id,
        consumeTapEvents: false,
        strokeColor: Color.fromRGBO(r, g, b, 100),
        strokeWidth: 1,
        fillColor: Color.fromRGBO(r, g, b, 0.1),
        points: latlang,
        onTap: () {
          //print("ID zona --> " + id.toString());
        },
      ));
    });
  }

  Future<void> validateCustomerZone(
      BuildContext context, latLngCustomer) async {
    for (Polygon item in _polygons) {
      if (await GoogleMapPolyUtil.containsLocation(
          polygon: item.points, point: latLngCustomer)) {
        showToast(
            context, "Dentro de la zona: " + item.polygonId.value.toString());
        break;
      }
    }
  }

  void showToast(BuildContext context, msg) {
    Toast.show(msg, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }

  void localizarme(){
    getLocalization();
  }

  Future<void> getLocalization() async {
    Location location = new Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    locationData = await location.getLocation();
    _goToTheLake(locationData);
  }

  Future<void> _goToTheLake(LocationData locationData) async {
    final GoogleMapController controller = await _controller.future;
    CameraPosition _kLake = CameraPosition(
        bearing: 192.8334901395799,
        target: LatLng(locationData.latitude, locationData.longitude),
        zoom: 14);
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
    _onAddMarker(LatLng(locationData.latitude, locationData.longitude), "cliente");
    validateCustomerZone(context, LatLng(locationData.latitude, locationData.longitude));
  }
}
